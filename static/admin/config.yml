backend:
  name: git-gateway
  branch: master

media_folder: static/img
public_folder: /img

display_url: https://thienlywebappdeveloper.netlify.com/

collections:
  - name: "projectitems"
    label: "Project items"
    description: "Project, feature, Date"
    folder: "src/pages/projectitems"
    create: true
    fields:
      - { label: "Template Key", name: "templateKey", widget: "hidden", default: "projectitem" }
      - { label: "Title", name: "title", widget: "string" }
      - { label: "Date", name: "date", widget: "datetime" }
      - {
          label: software Projects,
          name: softwareProjects,
          required: true,
          widget: list,
          fields:
            [
              { label: Image caption, name: imgCaption, required: true, widget: string },
              {
                label: Software Project Title,
                name: softwareProjectTitle,
                required: true,
                widget: string,
              },
              {
                label: Image,
                name: image,
                required: false,
                hint: "If an image isn't specified, a default headshot placeholder image will be used",
                widget: image,
              },
              { label: Description, name: body, required: true, widget: "markdown" },
              {
                label: Links,
                name: links,
                required: false,
                widget: list,
                fields:
                  [
                    { label: Text, name: linkText, widget: string },
                    { label: URL, name: linkURL, widget: string },
                  ],
              },
            ],
        }

  - name: "pages"
    label: "Pages"
    files:
      - file: "src/pages/about/index.md"
        label: "About"
        name: "about"
        fields:
          - { label: "Template Key", name: "templateKey", widget: "hidden", default: "about-page" }
          - { label: "Title", name: "title", widget: "string" }
          - {
              label: "Main Image",
              name: "mainImage",
              widget: "object",
              fields:
                [
                  { label: "Image", name: "image", widget: "image" },
                  { label: "Image Description", name: "imageAlt", widget: "string" },
                ],
            }
          - { label: "Body", name: "body", widget: "markdown" }
          - {
              label: "Image Gallery",
              name: "gallery",
              widget: "list",
              fields:
                [
                  { label: "Image", name: "image", widget: "image" },
                  { label: "Image Description", name: "imageAlt", widget: "string" },
                ],
            }
          - { label: "Details Info ", name: "detailsInfo", widget: "markdown" }
          - {
              label: "SEO & Meta",
              name: "seo",
              widget: "object",
              fields:
                [
                  { label: "Browser Tab Title", name: "browserTitle", widget: "string"},
                  { label: "Title", name: "title", widget: "string" },
                  { label: "Description", name: "description", widget: "string" },
                ],
            }
      - file: "src/pages/projects/index.md"
        label: "Projects"
        name: "projects"
        fields:
          - {
              label: "Template Key",
              name: "templateKey",
              widget: "hidden",
              default: "projects-page",
            }
          - { label: "Title", name: "title", widget: "string" }
          - { label: "Body", name: "body", widget: "markdown" }
          - {
              label: "SEO & Meta",
              name: "seo",
              widget: "object",
              fields:
                [
                  { label: "Browser Tab Title", name: "browserTitle", widget: "string"},
                  { label: "Title", name: "title", widget: "string" },
                  { label: "Description", name: "description", widget: "string" },
                ],
            }
      - file: "src/pages/home/index.md"
        label: "Home"
        name: "home"
        fields:
          - { label: "Template Key", name: "templateKey", widget: "hidden", default: "home-page" }
          - {
              label: "Header Image",
              name: "headerImage",
              widget: "object",
              fields:
                [
                  { label: "Image", name: "image", widget: "image" },
                  { label: "Image Description", name: "imageAlt", widget: "string" },
                ],
            }
          - { label: "Title", name: "title", widget: "string" }
          - { label: "Skills Heading", name: "skillHeading", widget: "string" }
          - {
              label: "Location",
              name: "location",
              widget: "object",
              fields:
                [
                  { label: "Map Latitude", name: "mapsLatitude", widget: "string"},
                  { label: "Map Longitude", name: "mapsLongitude", widget: "string" },
                  { label: "Map Link", name: "mapsLink", widget: "string" },
                  { label: "name", name: "name", widget: "string" }
                ],
            }
          - { label: "Maps Note", name: "mapsNote", required: true, widget: "string" }
          - {
              label: "Call to Actions",
              name: "callToActions",
              required: true,
              widget: "object",
              fields:
                [
                  {
                    label: "First CTA",
                    name: "firstCTA",
                    required: true,
                    widget: "object",
                    fields:
                      [
                        { label: "Heading", name: "heading", widget: "string" },
                        { label: "Sub Heading", name: "subHeading", widget: "string" },
                        {
                          label: "Link Type",
                          name: "linkType",
                          widget: "select",
                          options: ["internal", "external"],
                        },
                        {
                          label: "Link URL",
                          name: "linkURL",
                          widget: "string",
                          hint: "Use a relative URL (e.g. /about) if the link is an internal link.",
                        }
                      ]
                  },
                  {
                    label: "Second CTA",
                    name: "secondCTA",
                    required: true,
                    widget: "object",
                    fields:
                      [
                        { label: "Heading", name: "heading", widget: "string" },
                        { label: "Sub Heading", name: "subHeading", widget: "string" },
                        {
                          label: "Link Type",
                          name: "linkType",
                          widget: "select",
                          options: ["internal", "external"],
                        },
                        {
                          label: "Link URL",
                          name: "linkURL",
                          widget: "string",
                          hint: "Use a relative URL (e.g. /about) if the link is an internal link.",
                        }
                      ]
                  }
                ],
            }
          - {
              label: "SEO & Meta",
              name: "seo",
              widget: "object",
              fields:
                [
                  { label: "Browser Tab Title", name: "browserTitle", widget: "string"},
                  { label: "Title", name: "title", widget: "string" },
                  { label: "Description", name: "description", widget: "string" },
                ],
            }
  - name: "navbarAndFooter"
    label: "Navbar & Footer"
    files:
      - file: "src/pages/navbar/index.md"
        label: "Navbar"
        name: "navbar"
        fields:
          - { label: "Template Key", name: "templateKey", widget: "hidden", default: "navbar" }
          - {
              label: "Menu Items",
              name: "menuItems",
              widget: "list",
              fields:
                [
                  { label: "Label", name: "label", widget: "string" },
                  { label: "Link Type", name: "linkType", widget: "select", options: ["internal", "external"] },
                  { label: "Link URL", name: "linkURL", widget: "string", hint: "Use a relative URL (e.g. /about) if the link is an internal link." },
                ],
            }
      - file: "src/pages/footer/index.md"
        label: "Footer"
        name: "footer"
        fields:
          - { label: "Template Key", name: "templateKey", widget: "hidden", default: "footer" }
          - {
              label: "Logo Image & Tagline",
              name: "logoImage",
              widget: "object",
              fields:
                [
                  { label: "Image", name: "image", widget: "image" },
                  { label: "Image Description", name: "imageAlt", widget: "string" },
                  { label: "Tagline", name: "tagline", widget: "string" },
                ],
            }
          - {
              label: "Social Links",
              name: "socialLinks",
              widget: "list",
              fields:
                [
                  { label: "Image", name: "image", widget: "image" },
                  { label: "Image Description", name: "imageAlt", widget: "string" },
                  { label: "Label", name: "label", widget: "string" },
                  { label: "Link URL", name: "linkURL", widget: "string" },
                ],
            }