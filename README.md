
### Style guidline
#### Using Bem styles.
- Use the hyphen to name the block element and child element if necessary.
- className for Block element equals to block-element
- className for Child element would be block-element__child-element.(using double underscore to specify child element). 
- Notice:Avoid multiple element level naming, no nested child-element, in other words, grandchild selectors: block-element__child-element__grand-child-element . In stead, naming it likes this:
block-element__grand-child-element

- className for Modifier would be: block-element_modifier_name (using an underscore to specify modifier on the block element only)
```css
.block-element  {
    //style for the block element
    &__child-element  {
    //style for the child element
    }
}
.block-element_modifier_name  {
    //style for the block element
    &__child-element  {
    //style for the child element
    }
}

```

#### Since I use particles.js as background.
- Set the canvas z-index: -1, all other html tag z-index:1,
- Set the html tag position to relative to make the element background color or text visible
- Some section I have set the background color in order to see the text, since the color is white. In the cms preview feature, those texts can not be visible if the background is transparent.
- Those section needs transparent background, set the text tag like p, h1, ... to have relative position.
- Those section needs to have color background, set the section position to relative.
##### Example 
```css
#particles-js canvas {
    z-index: -1 !important;
}
* {
    z-index: 1;
}

html{
    background-color: #2d3047;
}
h1,h2,h3,h4,h5,h6,p,b,i,u,strike,pre,code,tt,blockquote,small,strong,em,center,font,a,li {
    position: relative;
}
```


# Gatsby & Netlify CMS 
Original template Gatsby & Netlify CMS from
[![Read the tutorial](https://drive.google.com/file/d/1douAi8UUwq9x2prmBxKP0phKr47-ne9w/view?usp=sharing)](https://blog.logrocket.com/gatsby-netlify-cms-a-perfect-pairing-d50d59d16f67)

You can easily deploy your own instance of my portfolio by clicking the button below:
[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/duongthienlee/cmsPortfolio)

## Local Development

### Prerequisites

- Node (see [.nvmrc](./.nvmrc) for version)

### Run the project

```
$ git clone https://github.com/duongthienlee/cmsPortfolio.git
$ cd cmsPortfolio
$ yarn
$ yarn develop
```

To test the CMS locally, you'll to need run a production build of the site:

```
$ yarn build
$ yarn serve
```

### Setting up the CMS

For details on how to configure the CMS, take a look at the [Netlify CMS Docs](https://www.netlifycms.org/docs/intro).

## Useful Ressources
- ["Official" Gatsby and Netlify CMS starter](https://github.com/netlify-templates/gatsby-starter-netlify-cms)
This starter includes a blog built with Gatsby and Netlify CMS. It was actually used as the starting off point for this repository.
