import React, { Component } from "react";
import MediaQuery from 'react-responsive';
import CustomLink from "../CustomLink";
import fbIcon from "../../../src/img/icons/facebook.svg"
import linkedin from "../../../src/img/icons/linkedin.svg"
import instagram from "../../../src/img/icons/instagram.svg"
let lastScrollTop = 0;

export class NavbarTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showNavBar: true,
      // mobile menu visibility when click the HamburgerBtn
      mobileMenuVisible: false,
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  showHamburgerMenu = () => {
    this.setState({ mobileMenuVisible: !this.state.mobileMenuVisible });
  }
  hideHamburgerMenu = () => {
    this.setState({ mobileMenuVisible: !this.state.mobileMenuVisible });
  }
  // when user click hamburgerBtn
  handleMouseUp = (event) => {
    this.showHamburgerMenu();
    event.stopPropagation();
    event.preventDefault()
  }
  handleScroll = (event) => {
    event.stopPropagation();
    event.preventDefault()
    let currentYOffset = window.pageYOffset || document.documentElement.scrollTop;

    if (currentYOffset >= 60 && currentYOffset > lastScrollTop) {
      /* downscroll code 
        console.log(" current " + currentYOffset + " prev " + lastScrollTop)
      */
      // If mobile menu is visble, always show nav bar even when scroll down
      this.state.mobileMenuVisible ?
        this.setState({ showNavBar: true }) : this.setState({ showNavBar: false })

    } else if (currentYOffset >= 60 && currentYOffset < lastScrollTop) {
      // upscroll code. 
      this.setState({ showNavBar: true })
    }
    // this to ensure the nav bar always show even currentYOffset =0
    else {
      this.setState({ showNavBar: true })
    }

    lastScrollTop = currentYOffset <= 0 ? 0 : currentYOffset; // For Mobile or negative scrolling
  };

  renderNavLi = (data, isMobile) => {
    return (
      <React.Fragment>
        {data.menuItems.length > 0 && (
          <ul className={`navbar-menu ${isMobile ? 'ul-mobile-navbar-menu' : ''}`}>
            {data.menuItems.map(menuItem => (
              <li key={menuItem.linkURL} className="navbar-menuItem">
                <CustomLink
                  activeClassName="navbar-menuLink--active"
                  linkType={menuItem.linkType}
                  linkURL={menuItem.linkURL}
                  className="navbar-menuLink"
                >
                  {isMobile ? <span> {menuItem.label}</span> : menuItem.label}
                </CustomLink>
              </li>
            ))}
          </ul>
        )}
      </React.Fragment>
    )
  }
  render() {
    const { data } = this.props
    return (
      <nav className={`navbar ${this.state.showNavBar ? 'navbar--show' : 'navbar--hide'} `}>
        <div className="navbar-container">
          <MediaQuery query="(min-device-width: 992px)">
            {this.renderNavLi(data, false)}
          </MediaQuery>
          <MediaQuery query="(max-device-width: 991px)">
            <div className={`hamburger-btn ${this.state.mobileMenuVisible ? "hamburger-btn--active" : ""}`}
              onMouseUp={this.handleMouseUp}>
              <div className="hamburger-btn__bar1"></div>
              <div className="hamburger-btn__bar2"></div>
              <div className="hamburger-btn__bar3"></div>
            </div>
            <div onClick={() => this.hideHamburgerMenu()} className={`mobile-navbar-menu ${this.state.mobileMenuVisible ? "mobile-navbar-menu--opened" : "mobile-navbar-menu--hide"}`} >
              <div className="center-vertical-mobile-nav" >
                {this.renderNavLi(data, true)}
                <div className="social-icon-nav"  >
                  <a href="https://www.facebook.com/winchesterthien" rel="noopener noreferrer" target="_blank">
                    <img className="social-icon" src={fbIcon} alt="fb-icon" />
                  </a>
                  <a href="https://www.linkedin.com/in/duongthienly" rel="noopener noreferrer" target="_blank">
                    <img className="social-icon" src={linkedin} alt="linkedin-icon" />
                  </a>
                  <a href="https://www.instagram.com/thienwinchester" rel="noopener noreferrer" target="_blank">
                    <img className="social-icon" src={instagram} alt="instagram-icon" />
                  </a>
                </div>
              </div>
            </div>
          </MediaQuery>
        </div>
      </nav >
    )
  }
}


const Navbar = props => {
  if (!props.data) {
    return null;
  }
  const data = props.data.edges[0].node.frontmatter;
  return <NavbarTemplate data={data} />;
};
export { Navbar };
