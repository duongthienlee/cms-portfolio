import React from 'react';
import requiredIf from '../Help/reactRequireIf';
import PropTypes from "prop-types";
const Header = ({ page, imgFloatLeft, isFrontmatter, headerClassModifier, children }) => {
    let string, imgSrc, imgAlt, imgOrder, tagLineOrder;
    if (!children) {
        string = isFrontmatter ? page.frontmatter.title : page.title
        imgSrc = isFrontmatter ? page.frontmatter.mainImage.image : page.headerImage.image
        imgAlt = isFrontmatter ? page.frontmatter.mainImage.imageAlt : page.headerImage.imageAlt
        imgOrder = imgFloatLeft ? 1 : 2
        tagLineOrder = imgFloatLeft ? 2 : 1
    }
    return (
        <header className={`header header_original_modifier ${headerClassModifier}`}>
            {!children ?
                <>
                    {isFrontmatter ?
                        <img style={{ order: imgOrder }} src={imgSrc} alt={imgAlt} />
                        : (page.headerImage && <img style={{ order: imgOrder }} src={imgSrc} alt={imgAlt} />)
                    }
                    <h1 style={{ order: tagLineOrder }} >
                        <span>
                            {string}
                        </span>
                    </h1>
                </>
                :
                children
            }
        </header>
    );
}
export default Header
Header.propTypes = {
    page: requiredIf(PropTypes.any, props => !props.children),
    imgFloatLeft: requiredIf(PropTypes.bool, props => !props.children),
    isFrontmatter: requiredIf(PropTypes.bool, props => !props.children),
    headerClassModifier: PropTypes.string,
    children: PropTypes.any
};
