---
templateKey: projects-page
title: Projects
path: /projects
seo:
  browserTitle: Projects Page
  description: View the projects that I have been doing
  title: Projects Page | Web Developer
---

Here are some of the project I have been doing so far. If you're interested in my those projects and my skills, contact me!
