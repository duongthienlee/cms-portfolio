---
templateKey: footer
logoImage:
  image: /img/mylogo.svg
  imageAlt: JavaScript
  tagline: Contact via my social media networks!
socialLinks:
  - image: /img/instagram.svg
    imageAlt: Follow me on Instagram
    label: Instagram
    linkURL: 'https://www.instagram.com/thienwinchester/'
  - image: /img/facebook.svg
    imageAlt: Add my Facebook
    label: Facebook
    linkURL: 'https://www.facebook.com/winchesterthien'
  - image: /img/linkedin.svg
    imageAlt: Add me on Linkedin
    label: Linkedin
    linkURL: 'https://www.linkedin.com/in/duongthienly'
---

