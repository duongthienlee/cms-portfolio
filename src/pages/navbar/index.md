---
templateKey: navbar
menuItems:
  - label: HOME
    linkType: internal
    linkURL: /
  - label: ABOUT
    linkType: internal
    linkURL: /about
  - label: PROJECTS
    linkType: internal
    linkURL: /projects
---

