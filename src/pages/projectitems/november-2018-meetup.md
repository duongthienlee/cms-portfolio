---
templateKey: projectitem
title: React Tic Tac Toe using React hooks
date: 2019-04-19T21:00:00.000Z
softwareProjects:
  - image: /img/screenshot-2019-04-24-at-0.15.19.png
    imgCaption: Tic Tac Toe game
    links:
      - linkText: Github
        linkURL: 'https://github.com/duongthienlee/react-hooks-tic-tac-toe'
      - linkText: Live
        linkURL: 'https://duongthienlee.github.io/react-hooks-tic-tac-toe/#/'
    softwareProjectTitle: Getting my feet wet with React hooks
---

 ## React tic tac toe with my solution using React Hooks, tutorial from
      https://reactjs.org/tutorial/tutorial.html#wrapping-up 

      Improvements that make the tic-tac-toe game which are listed in order of
      increasing difficulty from React

      1. Display the location for each move in the format (col, row) in the move
      history list.

      2. Bold the currently selected item in the move list.

      3. Rewrite Board to use two loops to make the squares instead of
      hardcoding them.

      4. Add a toggle button that lets you sort the moves in either ascending or
      descending order.

      5. When someone wins, highlight the three squares that caused the win.

      6. When no one wins, display a message about the result being a draw.


      ### Using React hooks {useState, useRef, useEffect}

      ### Highlight current move, some css style
