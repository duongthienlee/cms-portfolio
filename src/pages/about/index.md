---
templateKey: about-page
title: I am a Front End web developer!
mainImage:
  image: /img/53658706_562701107567551_8657581366294085632_o.jpg
  imageAlt: Thien Ly
gallery:
  - image: /img/15896003_121143335056666_500433035900544333_o.jpg
    imageAlt: This is me when I was a child.
  - image: /img/53658706_562701107567551_8657581366294085632_o.jpg
    imageAlt: This is me when I was traveling around Finland
  - image: /img/44668909_488267665010896_2192886589099606016_o.jpg
    imageAlt: This is me also
detailsInfo: |-
  ## Details Information About Me
  
  * Name:  Duong Thien Ly 
  * Age: 23
  * Location: Espoo, Finland
  * School: Haaga-Helia University of Applied Sciences

seo:
  browserTitle: About Me | Web App Developer
  description: About Me | Web App Developer
  title: About Me | Web App Developer
---
## About Me

I am a Business Information Technology student at Haaga-Helia UAS. Although, there are some business courses in my study programme, I tend to indulge in IT courses. I can spend hours or even a whole day just to code and design website. When I start a project, I usually overdo it, because I want to make awesome projects. That's how I learn new things.
