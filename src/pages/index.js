import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import Helmet from "react-helmet";
import "../styles/main.scss"
import Layout from "../components/Layout";
import Map from "../components/Map";
import HeadshotPlaceholder from "../img/headshot-placeholder.svg";
import CustomLink from "../components/CustomLink";

import Header from "../components/Header"
export const HomePageTemplate = ({ home, location }) => {

  const latitude = location && location.mapsLatitude;
  const longitude = location && location.mapsLongitude;
  const mapsLink = location && location.mapsLink;
  const name = location && location.name;
  return (
    <>
      <Header
        isFrontmatter={false}
        page={home}
        imgFloatLeft={true}
      />
      <div className="container">
        <section className="section project">
          <h2 className="project-title">{home.skillHeading}</h2>
        </section>

        <p className="mapNote">{home.mapsNote}</p>

        <section className="section section_no_padding_but_bottom mapWrapper">
          <Map
            isMarkerShown
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDw1A2Ay2ly_rWJuqjboxHv9OoVpr-KMSQ&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `100%` }} />}
            mapElement={<div style={{ height: `100%` }} />}
            link={mapsLink}
            latitude={latitude}
            longitude={longitude}
          />
        </section>
      </div>

      <section className="section ctaBlock section_fluid">
        <CustomLink
          linkType={home.callToActions.firstCTA.linkType}
          linkURL={home.callToActions.firstCTA.linkURL}
          className="ctaBlock-pattern  ctaBlock-pattern--first"
        >
          <div className="ctaBlock-cta">
            <span className="ctaBlock-ctaHeading">{home.callToActions.firstCTA.heading}</span>
            <p className="ctaBlock-ctaDescription">{home.callToActions.firstCTA.subHeading}</p>
          </div>
        </CustomLink>
        <CustomLink
          linkType={home.callToActions.secondCTA.linkType}
          linkURL={home.callToActions.secondCTA.linkURL}
          className="ctaBlock-pattern  ctaBlock-pattern--second"
        >
          <div className="ctaBlock-cta">
            <span className="ctaBlock-ctaHeading">{home.callToActions.secondCTA.heading}</span>
            <p className="ctaBlock-ctaDescription">{home.callToActions.secondCTA.subHeading}</p>
          </div>
        </CustomLink>
      </section>
    </>
  );
};

class HomePage extends React.Component {
  render() {
    const { data } = this.props;
    const {
      data: { footerData, navbarData },
    } = this.props;
    const { frontmatter: home } = data.homePageData.edges[0].node;
    const { seo: { title: seoTitle, description: seoDescription, browserTitle }, } = home;
    const { location } = home;
    return (
      <Layout footerData={footerData} navbarData={navbarData}>
        <Helmet>
          <meta name="title" content={seoTitle} />
          <meta name="description" content={seoDescription} />
          <title>{browserTitle}</title>
        </Helmet>
        <HomePageTemplate home={home} location={location} />
      </Layout>
    );
  }
}

HomePage.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.array,
    }),
  }),
};

export default HomePage;

export const pageQuery = graphql`
  query HomePageQuery {
        allMarkdownRemark(
          filter: {frontmatter: {softwareProjects: {elemMatch: {softwareProjectTitle: {ne: null } } } } }
      sort: {order: DESC, fields: frontmatter___date }
    ) {
        edges {
      node {
        frontmatter {
      title
      formattedDate: date(formatString: "MMMM Do YYYY @ h:mm A")
      rawDate: date
            softwareProjects {
              softwareProjectTitle
              imgCaption
              image        
    }
   
  }
}
}
}
...LayoutFragment
    homePageData: allMarkdownRemark(filter: {frontmatter: {templateKey: {eq: "home-page" } } }) {
        edges {
          node {
            frontmatter {
              title
              headerImage {
              image
              imageAlt
              }
            skillHeading
            location {
              mapsLatitude
              mapsLongitude
              mapsLink
              name
            }
            mapsNote
            callToActions {
              firstCTA {
                heading
                subHeading
                linkType
                linkURL
                }
                secondCTA {
                  heading
                  subHeading
                  linkType
                  linkURL
                }
              }
            seo {
              browserTitle
              title
              description
              }
        }
      }
}
}
}
`;
