---
templateKey: home-page
headerImage:
  image: /img/mylogo.svg
  imageAlt: Web developer
title: >-
  I'm Thien, a front end web app developer, also a fan of Supernatural (U.S. TV
  series)
skillHeading: Skills
location:
  mapsLatitude: 60.2031247
  mapsLongitude: 24.6534766
  mapsLink: 'https://thienlywebdeveloper.netlify.com/'
  name: Thien Ly Location 
mapsNote: Clicking the pin opens Google Maps in a new tab.
callToActions:
  firstCTA:
    heading: My Projects
    linkType: internal
    linkURL: /projects
    subHeading: Have a look at my projects.
  secondCTA:
    heading: Let build awesome websites!
    linkType: external
    linkURL: 'mailto:duongthienlee@gmail.com'
    subHeading: 'Contact me, I will response asap.'
seo:
  browserTitle: Thien Ly Web Developer
  description: >-
    I am a Web App developer. I am familiar with React, React-native, flexbox,
    sass, less, AWS...
  title: Web Developer
---

