import React from "react";
import PropTypes from "prop-types";
import format from "date-fns/format";

import ProjectTemplate from "../../templates/ProjectTemplate";

const ProjectPreview = ({ entry }) => {
  const project = entry.getIn(["data"]).toJS();
  const rawDate = project.date;
  const formattedDate = format(rawDate, "MMMM Do YYYY @ h:mm A");
  return <ProjectTemplate project={{ ...project, formattedDate, rawDate }} />;
};

ProjectPreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
};

export default ProjectPreview;
