import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import Helmet from "react-helmet";
import ReactMarkdown from "react-markdown";
import "../styles/main.scss"
import ProjectTemplate from "./ProjectTemplate";
import Layout from "../components/Layout";
import HTMLContent from "../components/Content";
import Header from "../components/Header"
export const ProjectsPageTemplate = ({
  title,
  content,
  projects = null,
  bodyIsMarkdown = false
}) => {
  return (
    <article className="my-projects">
      <Header headerClassModifier="header_project_page">
        <h1>{title}</h1>
        {bodyIsMarkdown ? (
          <ReactMarkdown className="my-projects-description" source={content} />
        ) : (
            <HTMLContent className="my-projects-description" content={content} />
          )}
      </Header>
      {projects &&
        projects.map((project, index) => (
          <ProjectTemplate
            bodyIsMarkdown={bodyIsMarkdown}
            key={index}
            className="my-projects-project"
            project={project.node.frontmatter}
          />
        ))}
    </article>
  );
};

ProjectsPageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  projects: PropTypes.array,
};

const ProjectsPage = ({ data }) => {
  const { markdownRemark: page } = data;
  const {
    frontmatter: {
      seo: { title: seoTitle, description: seoDescription, browserTitle },
    },
  } = page;
  let projects = data.allMarkdownRemark.edges;


  return (
    <Layout footerData={data.footerData} navbarData={data.navbarData}>
      <Helmet>
        <meta name="title" content={seoTitle} />
        <meta name="description" content={seoDescription} />
        <title>{browserTitle}</title>
      </Helmet>
      <ProjectsPageTemplate
        title={page.frontmatter.title}
        content={page.html}
        projects={projects}
      />
    </Layout>
  );
};

ProjectsPage.propTypes = {
  data: PropTypes.object.isRequired,
};

export default ProjectsPage;

export const ProjectsPageQuery = graphql`
  query ProjectsPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        title
        seo {
          browserTitle
          title
          description
        }
      }
    }
    ...LayoutFragment
    allMarkdownRemark(
      filter: { frontmatter: { softwareProjects: { elemMatch: { softwareProjectTitle: { ne: null } } } } }
      sort: { order: DESC, fields: frontmatter___date }
    ) {
      edges {
        node {
          frontmatter {
            title
            formattedDate: date(formatString: "MMMM Do YYYY @ h:mm A")
            rawDate: date
            softwareProjects {
              softwareProjectTitle
              image
              imgCaption
              links {
                linkText
                linkURL
              }
            }
          }
        }
      }
    }
  }
`;
