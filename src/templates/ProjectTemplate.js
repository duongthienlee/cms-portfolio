import React, { Component } from "react";
import PropTypes from "prop-types";
import ReactMarkdown from "react-markdown";
import HTMLContent from "../components/Content";
import HeadshotPlaceholder from "../img/headshot-placeholder.svg";
import "../styles/main.scss"
class ProjectTemplate extends Component {
  render() {
    return (
      <div className="container">
        <section
          className={`section project`}
          key={this.props.project.rawDate}
        >
          <h2 className="project-title">{this.props.project.title}</h2>
          <div className="project-meta">
            <p className="project-metaField project-metaField--date">
              <span className="project-label">Date:</span> {this.props.project.formattedDate}
            </p>
          </div>
          <div className="project-info-wrapper">
            {this.props.project.softwareProjects.map(softwareProject => (
              <div className="project-info" key={softwareProject.softwareProjectTitle}>
                <div className="project-info__ImageContainer">
                  <center>
                    <img
                      className="ImageContainer__img"
                      src={softwareProject.image ? softwareProject.image : HeadshotPlaceholder}
                      alt={softwareProject.image ? softwareProject.imgCaption : "Default headshot placeholder"}
                    />
                    <center className="ImageContainer__infoName">{softwareProject.imgCaption}</center>
                  </center>
                </div>
                <div className="project-info__description">
                  {softwareProject.softwareProjectTitle && (
                    <h3 className="description__infoTitle">{softwareProject.softwareProjectTitle}</h3>
                  )}
                  {this.props.bodyIsMarkdown ? (
                    <ReactMarkdown className="description__infoText" source={softwareProject.body} />
                  ) : (
                      <HTMLContent className="description__infoText" content={softwareProject.body} />
                    )}
                  <ul className="description__infoLinks">
                    {softwareProject.links &&
                      softwareProject.links.map((link, index) => (
                        <li key={index} className="infoLinks__infoLinkItem">
                          <a className="infoLinks__infoLink" href={link.linkURL}>
                            {link.linkText}
                          </a>
                        </li>
                      ))}
                  </ul>
                </div>
              </div>
            ))}
          </div>
        </section>
      </div>
    );
  }
}

ProjectTemplate.propTypes = {
  project: PropTypes.shape({
    title: PropTypes.string,
    bodyIsMarkdown: PropTypes.bool,
    softwareProjects: PropTypes.array,
  }),
};

export default ProjectTemplate;
