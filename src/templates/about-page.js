import React from 'react';
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import ReactMarkdown from "react-markdown";
import Helmet from "react-helmet";
import Layout from '../components/Layout';
import HTMLContent from '../components/Content';
import Header from "../components/Header"
import "../styles/main.scss"
export const AboutPageTemplate = props => {
  const { page } = props;

  return (
    <article className="about">
      <Header
        page={page}
        imgFloatLeft={false}
        isFrontmatter={true}
        headerClassModifier="header_about_page"
      />

      <div className="container">
        <section className="section section_about__me section_no_padding_but_bottom">
          {/* The page.html is actually markdown when viewing the page from the netlify CMS,
              so we must use the ReactMarkdown component to parse the mardown in that case  */}
          {page.bodyIsMarkdown ? (
            <ReactMarkdown className="about__description" source={page.html} />
          ) : (
              <HTMLContent className="about__description" content={page.html} />
            )}
          <ul className="about__gallery">
            {page.frontmatter.gallery.map((galleryImage, index) => (
              <li key={index} className="about__list-item">
                <img src={galleryImage.image} alt={galleryImage.imageAlt} />
              </li>
            ))}
          </ul>
        </section>

        <section className="section section_about__details section_no_padding_but_bottom section_width">
          <ReactMarkdown source={page.frontmatter.detailsInfo} />
        </section>

      </div>
    </article>
  );
};

const AboutPage = ({ data }) => {
  const { markdownRemark: page, footerData, navbarData } = data;
  const {
    frontmatter: {
      seo: { title: seoTitle, description: seoDescription, browserTitle },
    },
  } = page;

  return (
    <Layout footerData={footerData} navbarData={navbarData}>
      <Helmet>
        <meta name="title" content={seoTitle} />
        <meta name="description" content={seoDescription} />
        <title>{browserTitle}</title>
      </Helmet>
      <AboutPageTemplate page={{ ...page, bodyIsMarkdown: false }} />
    </Layout>
  );
};

AboutPage.propTypes = {
  data: PropTypes.object.isRequired,
};

export default AboutPage;

export const aboutPageQuery = graphql`
  query AboutPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        title
        mainImage {
          image
          imageAlt
        }
        gallery {
          image
          imageAlt
        }
        detailsInfo
        seo {
          browserTitle
          title
          description
        }
      }
    }
    ...LayoutFragment
  }
`;
